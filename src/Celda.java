import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class Celda extends JComponent implements Constantes {
        public int x, y;
        //tipo: guardara un caracter sobre el tipo de celda que se define
        public char tipo;
        public BufferedImage jugador,energia,camino,termino,adversario;

        
        //constructor
        public Celda(int x,int y,char tipo) {
            this.x=x;
            this.y=y;
            this.tipo=tipo;

            try {
            
            camino = ImageIO.read(new File("images/camino.png"));
            jugador= ImageIO.read(new File("images/jugador.png"));
            energia=ImageIO.read(new File("images/final.png"));
            adversario= ImageIO.read(new File("images/adversario.png"));
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
        //metodo para dibujar celda, hace uso de drawRect
        @Override
        public void update(Graphics g){
            switch(tipo) {
          
            case 'C': g.drawImage(camino,x,y, this); break;
            case 'J': g.drawImage(jugador,x,y, this); break;
            case 'E': g.drawImage(energia,x,y, this); break;
            case 'A': g.drawImage(adversario,x,y, this); break;
            case 'V': g.setColor(COLORFONDO);
            g.fillRect(x, y,anchuraCelda,alturaCelda);
            break;
            }
        }
        
        @Override
        public void paintComponent(Graphics g) {
            update(g);
        }
     
        
}