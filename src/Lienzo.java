import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.util.Timer;


/**
 *
 * @author Tomas Salgado
 */
public class Lienzo extends Canvas implements Constantes{
    public Escenario laberinto;
    public Image fondo;
    //para implementar el doble buffer para que no se repinten los elementos
    public Graphics graficoBuffer;
    public Image imagenBuffer;
    //entidades del juego
    public Jugador jugador;
    public Adversario adversario;
    public Celda celdaPared, celdaFinal;
    public String numX,numY;
    public Timer lanzador1;
    public String movimientos[]= {"Izquierda","Izquierda","Izquierda","Izquierda","Arriba",
   "Abajo","Derecha","Abajo","Derecha","Arriba"};
    public Energia energia,energia1,energia2,energia3;
    public int contador,posicionX,posicionY,velocidad;
    public Reloj reloj;
    public Lienzo(){
        laberinto=new Escenario(this);
        energia= new Energia(laberinto,3,3);
        energia1=new Energia(laberinto,7,2);
        energia2=new Energia(laberinto,3,1);
        energia3=new Energia(laberinto,5,5);
        
        //color de fondo
        this.setBackground(Color.black);
        //dimensiones del lienzo
        this.setSize(laberinto.anchuraLaberinto,laberinto.alturaLaberinto);
               
        //escuchador eventos de teclado
        addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                
                laberinto.moverCelda(e);//lanzamos el metodo moverCelda de la clase Laberinto
                repaint();//se actualiza el lienzo
            }
        });
        velocidad =2000;
        //colocarPared();
        lanzador1 = new Timer();
        jugador = new Jugador(laberinto,0);
        
        lanzador1.scheduleAtFixedRate(jugador,0,velocidad);  
        
        contador=10;   
        posicionX=70;
        posicionY=20;
        
        reloj = new Reloj(laberinto);
         // Aquí se pone en marcha el timer cada segundo.
        Timer timer = new Timer();
        // Dentro de 0 milisegundos avísame cada 1000 milisegundos
        timer.scheduleAtFixedRate(reloj, 2000, 5000);
        
    }
    
     

      
    public void moverD(){
        
        posicionX=posicionX+70;
    }
    public void moverI(){
        posicionX=posicionX-70;
    }
    public void moverUp(){
        posicionY=posicionY-70;
    }
    public void moverAbajo(){
        posicionY=posicionY+70;
    }
    public void aumentaEnergia(){
        contador=contador+10;
    }
    
    public void restarEnergia(){
        
        contador=contador-1;
        if(contador==0){
            System.out.println("se termino el juego");
        }
    }
    
    

    
                   
    @Override
    public void update(Graphics g) {
        //inicialización del buffer gráfico mediante la imagen
        if(graficoBuffer==null){
            imagenBuffer=createImage(this.getWidth(),this.getHeight());
            graficoBuffer=imagenBuffer.getGraphics();
        }
        //volcamos color de fondo e imagen en el nuevo buffer grafico
        graficoBuffer.setColor(getBackground());
        graficoBuffer.fillRect(0,0,this.getWidth(),this.getHeight());
        graficoBuffer.drawImage(fondo, 0, 0, null);
        laberinto.update(graficoBuffer);
         //pintamos la imagen previa
        g.drawImage(imagenBuffer, 0, 0, null);
        
        g.drawString(""+contador,posicionX,posicionY);
    }
    
    //metodo llamada la primera que se pinta
    @Override
    public void paint(Graphics g) {
        update(g);
    }
    
   
  
}
    