
import java.util.TimerTask;


public class Jugador extends TimerTask implements Constantes{
    public Escenario escenario;
    public Celda celdaJugador;
    public String movimientos[];
    public int i=0;
    public boolean sentidoDerecho;
    public int tiempoDormido;

    public Jugador(Escenario escenario, int tiempo){
        this.tiempoDormido=tiempo;
        this.escenario=escenario;
        celdaJugador=new Celda(0,0,'J');
        this.escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
        sentidoDerecho=true;
        
    }
    public void moverArriba(){
        escenario.lienzoPadre.moverUp();
        if(celdaJugador.y>0){
            if(escenario.celdas[celdaJugador.x][celdaJugador.y-1].tipo!='A'){
                if(escenario.celdas[celdaJugador.x][celdaJugador.y-1].tipo!='E'){
                    if(escenario.celdas[celdaJugador.x][celdaJugador.y-1].tipo!='F'){
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                        celdaJugador.y=celdaJugador.y-1;
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    }else{
                        System.exit(0);
                    }
                }else{
                    escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                    celdaJugador.y=celdaJugador.y-1;
                    escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    escenario.lienzoPadre.aumentaEnergia();
                }    
            }                    
        }
    }
    
    public void moverAbajo(){
        escenario.lienzoPadre.moverAbajo();
        if(celdaJugador.y<9){
            if(escenario.celdas[celdaJugador.x][celdaJugador.y+1].tipo!='A'){
                if(escenario.celdas[celdaJugador.x][celdaJugador.y+1].tipo!='E'){
                    if(escenario.celdas[celdaJugador.x][celdaJugador.y+1].tipo!='F'){
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                        celdaJugador.y=celdaJugador.y+1;
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    }else{
                        System.exit(0);
                    }
                }else{
                    escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                    celdaJugador.y=celdaJugador.y+1;
                    escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    escenario.lienzoPadre.aumentaEnergia();
                }
            }            
        }
    }
    public void moverDerecha(){
        escenario.lienzoPadre.moverD();
        if(celdaJugador.x<9){
            if(escenario.celdas[celdaJugador.x+1][celdaJugador.y].tipo!='A'){
                if(escenario.celdas[celdaJugador.x+1][celdaJugador.y].tipo!='E'){
                    if(escenario.celdas[celdaJugador.x+1][celdaJugador.y].tipo!='F'){
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                        celdaJugador.x=celdaJugador.x+1;
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    }else{
                        System.exit(0);
                    }
                }else{
                    escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                    celdaJugador.x=celdaJugador.x+1;
                    escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    escenario.lienzoPadre.aumentaEnergia();
                } 
            }else{
                sentidoDerecho=false;
            }            
        }
    }
    
    public void moverIzquierda(){
        escenario.lienzoPadre.moverI();
        if(celdaJugador.x>0){
            if(escenario.celdas[celdaJugador.x-1][celdaJugador.y].tipo!='A'){
                if(escenario.celdas[celdaJugador.x-1][celdaJugador.y].tipo!='E'){
                    if(escenario.celdas[celdaJugador.x-1][celdaJugador.y].tipo!='F'){
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                        celdaJugador.x=celdaJugador.x-1;
                        escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                    }else{
                        System.exit(0);
                    
                    }
                }else{
                    escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='C';
                   celdaJugador.x=celdaJugador.x-1;
                     escenario.celdas[celdaJugador.x][celdaJugador.y].tipo='J';
                     escenario.lienzoPadre.aumentaEnergia();
                }
            }            
        }
    }   

    @Override
    public void run() {
        if(sentidoDerecho==true){
            moverDerecha();
        }else{
            moverIzquierda();
        }        
        try {
            Thread.sleep(tiempoDormido);
            System.out.println("velocidad"+escenario.lienzoPadre.velocidad);
            System.out.println("tiempoDormido:"+tiempoDormido);
        } catch (InterruptedException ex) {
        }
        escenario.lienzoPadre.repaint();
       
    }       
}
