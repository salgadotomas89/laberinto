/**
 *
 * @author Tomas Salgado
 */
public class Energia {
    public Escenario laberinto;
    public Celda portal;
    public int x,y;
    
    public Energia(Escenario laberinto,int x, int y){
        this.laberinto=laberinto;
        this.x=x;
        this.y=y;
        portal=new Celda(x,y,'E');
        laberinto.celdas[portal.x][portal.y].tipo='E';
    }
}
