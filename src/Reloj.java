
import java.util.TimerTask;

/**
 *
 * @author Tomas Salgado
 */
public class Reloj extends TimerTask{
    public Escenario e;
    public Reloj(Escenario e){
        this.e=e;
    }
    
    @Override
    public void run() {
        e.lienzoPadre.restarEnergia();
        e.lienzoPadre.repaint();
    }
    
}
