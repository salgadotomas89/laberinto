
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;

/**
 *
 * @author Tomas Salgado
 */
public class Escenario extends JComponent implements Constantes {
    public int anchuraLaberinto,alturaLaberinto;//dimensiones del laberinto
    public Celda[][] celdas;//las casillas n x m
    public Lienzo lienzoPadre;
    public int vel;
    
    public Escenario(Lienzo lienzopadre) {
        this.lienzoPadre=lienzopadre;
        celdas=new Celda[anchuraMundoVirtual][alturaMundoVirtual];
        vel=1000;
        //inicializar el array de celdas
        for(int i=0; i < anchuraMundoVirtual; i++)
            for ( int j=0 ; j < alturaMundoVirtual ; j++)              
                celdas[i][j]=new Celda(i+(i*anchuraCelda),j+(j*alturaCelda),'C');
                
                
                //ancho y largo del laberinto
                this.anchuraLaberinto=anchuraMundoVirtual*anchuraCelda;
                this.alturaLaberinto=alturaMundoVirtual*alturaCelda;
                this.setSize(anchuraLaberinto,alturaLaberinto);
                
                
    }
        

    @Override
    public void update(Graphics g) {
        for(int i=0; i < anchuraMundoVirtual ; i++)
            for ( int j=0 ; j < alturaMundoVirtual; j++)
                celdas[i][j].update(g);
    }
        
    @Override
    public void paintComponent(Graphics g) {
        update(g);
    }
        
        
        
    public void moverCelda( KeyEvent evento ) {
        switch( evento.getKeyCode() ) {
        case KeyEvent.VK_UP:
            lienzoPadre.jugador.moverArriba();            
        break;
        
        case KeyEvent.VK_DOWN:
            lienzoPadre.jugador.moverAbajo();
        break;
        case KeyEvent.VK_LEFT:
            lienzoPadre.jugador.moverIzquierda();
        break;
        case KeyEvent.VK_RIGHT:
            lienzoPadre.jugador.moverDerecha();
        break;
        case KeyEvent.VK_MINUS://cuando apreto la tecla menos decremento la vel de 100 en 100
            vel=vel+1000;//en realidad para ir mas lento se debe sumar para q aumente el tiempo de ejecucion
            lienzoPadre.jugador.tiempoDormido=  vel;  
            System.out.println("vel:"+vel);
        break;
        }
        
    }
              
}