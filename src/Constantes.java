
import java.awt.Color;
import java.util.Random;

/**
 *
 * @author Tomas Salgado
 */
public interface Constantes {
    public final int anchuraCelda=60;
    public final int alturaCelda=60;
    public final int anchuraMundoVirtual=10;
    public final int alturaMundoVirtual=10;
    //Para manejar los tipos de celdas
    public final char JUGADOR='J';
    public final char CAMINO='V';
    public final char ENERGIA='E';
    public final char FINAL='F';
    public final char ADVERSARIO='A';
    public final Color COLOR_JUGADOR= Color.BLUE;
    public final int ALFA=50;
    public final Color COLORFONDO=new Color(153,217,234,ALFA);
    default int numeroAleatorio(int minimo, int maximo) {
        Random random = new Random();
        int numero_aleatorio = random.nextInt((maximo - minimo) + 1) + minimo;
        return numero_aleatorio;
    }
}
