import java.util.TimerTask;

/**
 *
 * @author Tomas Salgado
 */
public class Adversario extends TimerTask implements Constantes {
    public Escenario escenario;
    public Celda celdaAdversario;
    public Boolean moverIzquierdo;
    
    public Adversario(Escenario escenario, int x,int y){
        this.escenario=escenario;
        celdaAdversario=new Celda(x,y,'A');
        this.escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
        moverIzquierdo=true;//variable que se inicializa en verdadero para saber q se mueve de derecha a izquierda
    }
    public void moverArriba(){
        if(celdaAdversario.y>0){
            if(escenario.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo!='J'){
                if(escenario.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo!='P'){
                    if(escenario.celdas[celdaAdversario.x][celdaAdversario.y-1].tipo!='F'){
                        escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='C';
                        celdaAdversario.y=celdaAdversario.y-1;
                        escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                    }else{
                        System.exit(0);
                    }
                } 
            }                       
        }
    }
    
    public void moverAbajo(){
        if(celdaAdversario.y<9){
            if(escenario.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo!='J'){
                if(escenario.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo!='P'){
                if(escenario.celdas[celdaAdversario.x][celdaAdversario.y+1].tipo!='F'){
                escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='C';
                celdaAdversario.y=celdaAdversario.y+1;
                escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                }else{
                    System.exit(0);
                }
            }
            }            
        }
    }
    public void moverDerecha(){
        if(celdaAdversario.x<9){
            if(escenario.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo!='J'){
                if(escenario.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo!='P'){
                if(escenario.celdas[celdaAdversario.x+1][celdaAdversario.y].tipo!='F'){
                    escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='C';
                    celdaAdversario.x=celdaAdversario.x+1;
                escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                }else{
                    System.exit(0);
                }
            }
            }           
        }
    }
    
    public void moverIzquierda(){
        if(celdaAdversario.x>0){
            if(escenario.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo!='J'){//si la cellda de adelnte es distinta de jugador
                if(escenario.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo!='P'){
                    if(escenario.celdas[celdaAdversario.x-1][celdaAdversario.y].tipo!='F'){
                        escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='C';
                        celdaAdversario.x=celdaAdversario.x-1;
                        escenario.celdas[celdaAdversario.x][celdaAdversario.y].tipo='A';
                    }else{
                        System.exit(0);                    
                    }
                }
            }else{//cuando esta de frente al jugador se cambia el sentido 
                moverIzquierdo=false;
            }            
        }
    }

    @Override
    public void run() {
        if(moverIzquierdo==true){//si la variable esta en verdadero movemos todo el tiempo hacia la izquierda
            moverIzquierda();
        }else{//si esta en falso movemos hacia atras
            moverDerecha();
        }
        escenario.lienzoPadre.repaint();
    }

    
}
